import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Navbar from "./components/navbar.component";
import Appbar from "./components/appbar.component";
import JobList from "./components/jobs-list.component";
import Job from "./components/job.component"
import Grid from '@material-ui/core/Grid';
import CreateJob from "./components/create-job.compontnet";
import NewJob from "./components/new-job.component";
import Profile from "./components/profile.component"
import { makeStyles } from '@material-ui/core/styles';
import ViewInquieries from "./components/view_inquieries.component"


const useStyles = makeStyles((theme) => ({

}));

function App() {

  const classes = useStyles();

  return (
    <div>
    <Router>
      
        
        
<Appbar/>
      <br/>
        <Route path="/" exact component={JobList} />
        <Route path="/add/" exact component={CreateJob} />  
        <Route path="/profile/" exact component={Profile} /> 
        <Route path="/new/" exact component={NewJob} />
        <Route path="/view-inquieries/" exact component={ViewInquieries} />
        


    </Router>    
    </div>
  );
}

export default App;
