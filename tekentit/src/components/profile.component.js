import React, { useState } from 'react';
import { makeStyles} from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import UpdateIcon from '@material-ui/icons/Update';
import Divider from '@material-ui/core/Divider';


const useStyles = makeStyles((theme) => ({
    root:{
        
        direction:"rtl",
        marginTop:25,
        marginRight:15,
        marginBottom: 25,
        marginLeft:50
        //marginLeft: theme.spacing(120),
    },
    title:{
        justifyContent:'center',
        flex:1,
        alignItems:'center',
        display:'flex',
        flexDirection:'column'
    },
    picture:{
        height: '190px',
        width: '190px',
        verticalAlign: 'center',
    },
    myData:{
        color: '#5E98C4',
        fontSize: 40,
        fontWeight: "bold",
        marginTop: 60
    },
    name:{
        color:'black',
        fontWeight:'bold',
        fontSize:45
    },
    datas:{
        color: 'color',
        fontSize: 25, 
        fontWeight: 350
    },
    netounim:{
        paddingRight: 35,
    },
    icon_button:{
        display:'flex',
        flexDirection: 'row-reverse',
        justifyContent:'space-between',
        paddingRight:1050
    },
    accord:{
        marginTop:50,
    },
    files:{
        color: 'black',
        fontWeight:'bold',
        fontSize:25
    },
    fle:{
        color:'black',
        direction:'rtl',
        fontSize:25,
        fontWeight:350
    },
    lst:{
        display:'flex',
        direction:'rtl',
        flexDirection:'column', 
    }
}));


const Profile = () => {
    const classes = useStyles();


    return (
        <div className={classes.root}>
            <div className={classes.title}>
                <Avatar src="./pictures/telechargement.jpg" className={classes.picture}/>
                <h1 className={classes.name}>
                    יוסי כהן
                </h1>
            </div>
            <div>
                <h2 className={classes.myData}>
                    נתונים שלי:
                </h2>
                <div className={classes.netounim}>
                    <h3 className={classes.datas}>
                        תפקיד נוכחי: רמ"ד דיסנילנד פריז
                    </h3>
                    <Divider/>
                    <h3 className={classes.datas}>
                        השכלה: תואר ראשון במנהל עסקים
                    </h3>
                    <Divider/>
                    <h3 className={classes.datas}>
                        ניסיון: מתפעל מתקנים בכיר
                    </h3>
                    <Divider/>
                    <h3 className={classes.datas}>
                        טלפון: 0558463214
                    </h3>
                    <Divider/>
                    <h3 className={classes.datas}>
                        מיקום מיגורים: חיפה
                    </h3>
                    <Accordion className={classes.accord}>
                        <AccordionSummary 
                             expandIcon = {<ArrowDropDownIcon/>}>
                               <Typography className={classes.files}> הקבצים שלי </Typography> 
                        </AccordionSummary>
                        <AccordionDetails>
                            <div className={classes.lst}>
                                <List>
                                     <ListItem className={classes.files}>
                                            <ListItemText className={classes.fle} primary = "קורות חיים" />
                                            <ListItemSecondaryAction className={classes.icon_button}>
                                                <IconButton edge="end" aria-label="delete">
                                                  <DeleteIcon />
                                                </IconButton>
                                                <IconButton edge="end" aria-label="update">
                                                  <UpdateIcon />
                                                </IconButton>
                                            </ListItemSecondaryAction>
                                     </ListItem>
                                     <ListItem>
                                            <ListItemText className={classes.fle} primary = "תעודות" />
                                            <ListItemSecondaryAction className={classes.icon_button}>
                                                <IconButton edge="end" aria-label="delete">
                                                  <DeleteIcon />
                                                </IconButton>
                                                <IconButton edge="end" aria-label="update">
                                                  <UpdateIcon />
                                                </IconButton>
                                            </ListItemSecondaryAction>
                                     </ListItem>
                                </List>
                            </div>
                        </AccordionDetails>
                    </Accordion>
                </div>
            </div>
        </div>
    )
}

export default Profile;