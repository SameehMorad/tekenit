import React, { Component, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import IconButton from '@material-ui/core/IconButton';
import CardHeader from '@material-ui/core/CardHeader';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import InfoIcon from '@material-ui/icons/Info';
import Divider from '@material-ui/core/Divider';



const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    marginLeft: 15,
    marginBottom: 15
  },
  media: {
    height: 140,
  },
  btn: {
    backgroundColor:'#db8266',
    marginRight: theme.spacing(15)
  },
  top_case:{
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-around',
    marginRight:80,
    marginLeft:80,
  },
  icon_btn:{
    edge:'end',
    size:'large',
    //color:'disabled'
  },
  tle:{
    fontWeight:'bold',
    alignItems:'center',
    justifyContent:'center',
    display:'flex',
    flex:1
  },
  style:{
    //backgroundColor:'#bdbdbd'
  }
}));

const Job = props => {
  const classes = useStyles();

  return (
    <Card className={classes.root} dir="rtl">

      <CardActionArea>

        <CardContent>
          <div className={classes.style} >
            <Typography edge="end" variant="h5" component="h2" className={classes.tle} >
              {props.title}
              </Typography>
            <div className={classes.top_case}>
              <IconButton className={classes.icon_btn}>
                <StarBorderIcon />
              </IconButton>
              <IconButton color="inherit" className={classes.icon_btn}>
                <InfoIcon />
              </IconButton>
            </div>
          </div>
          <Divider />
          <Typography variant="body2" color="textSecondary" component="p">
            {props.content}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button className={classes.btn} variant="contained">הגש\י מועמדות</Button>
      </CardActions>
    </Card>
  )
}

export default Job;
