import React, { Component, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import IconButton from '@material-ui/core/IconButton';
import CardHeader from '@material-ui/core/CardHeader';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import InfoIcon from '@material-ui/icons/Info';
import { Link } from 'react-router-dom';
import Divider from '@material-ui/core/Divider';


const useStyles = makeStyles((theme) => ({
    root: {      maxWidth: 345,
      marginLeft: 15,
      marginBottom: 15
    },
    media: {
      height: 140,
    },
    btn:{
        marginRight: theme.spacing(1)
    }
  }));

const AdminJob = props => {
    const classes = useStyles();

    return (
        <Card className={classes.root} dir="rtl">
            
      <CardActionArea>

        <CardContent>
        <div className={classes.style} >
            <Typography edge="end" variant="h5" component="h2" className={classes.tle} >
            {props.title}
              </Typography>
            <div className={classes.top_case}>
              <IconButton className={classes.icon_btn}>
                <StarBorderIcon />
              </IconButton>
              <IconButton color="inherit" className={classes.icon_btn}>
                <InfoIcon />
              </IconButton>
            </div>
          </div>
          <Divider />
 
          <Typography variant="body2" color="textSecondary" component="p">
          {props.content}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button className={classes.btn} variant="contained">מחק תקן</Button>
        <Button className={classes.btn} variant="contained">ערוך</Button>
        <Link to={"/view-inquieries/"}>
        <Button className={classes.btn} variant="contained">הצג פניות</Button>
        </Link>
      </CardActions>
    </Card>
    )
}

export default AdminJob;