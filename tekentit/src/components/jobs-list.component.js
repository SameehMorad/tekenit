import React, { Component, useState, useEffect } from 'react';
import {  fade, makeStyles } from '@material-ui/core/styles';
import Job from "../components/job.component";
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import TextField from '@material-ui/core/TextField';


const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'center',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
    },
    
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight:600,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
  bla: {
    display:'flex',
    flexDirection:'row',
  marginBottom:25
  }
  }));

const JobsList = () => {
    const classes = useStyles();

    const [jobs, setJobs] = useState([]);


    return (
        <div> 
          <div className={classes.bla}>
          <div className={classes.search}  color="#6bd4db">
        <div className={classes.searchIcon}  color="#6bd4db">
          <SearchIcon />
        </div>
        <TextField id="outlined-basic" label="Search" variant="outlined" />
        
      </div>
      </div>
             <GridList cols={3} 
             className={classes.root}
          >
                 <Job title="ראש צוות פיתוח" content="עיסוק הצוות העיקרי מיפוי ולוגיקות על מפות, בנוסף לכך פיתוח ויב בטכנולגיות חדשניות"/>
                 <Job title="קצין פרויקט" content="קצין פרויקט הכי חשוב בצבא שמטרתו תכלס כלום, אני ממשיך לכתוב כי אני צריך מלל ואני עדיין כותה תודה"/> 
                  <Job title="מנהל מוצר" content="המוצר הכי שווה מבין כל המוצרים האחרים שווה לך לבוא אלינו ולהתרשם מהשירות האיכות והמענה המהיר תודה"/>
                 <Job title="ראש צוות דאטה" content="כמה דאטה מלא דברים מגניבים בעיקר דאטה ועוד דאטה ועוד דאטה ועוד דאטה זהו יש מספיק מלל"/>
                 <Job title="ראש שצוות Data Analyst" content="תפקיד בתחום הדאטה. המטרה לדעת את כל המידה שצריך"/>
                 <Job title="P.M" content="ניהוך ושיווק מוצרים ללקוחות שלנו שלא עונים אף פעם ואף פעם לא מרוצים ממה שאתה מגיש להם"/>
                 <Job title="תכניתן" content="בתפקיד חלומות זה אתה תתבקש לכתוב מיליוני שורות קוד ואוךי אתה אפילו תוקפץ ליחיד בזמן שאתה בקורס קצינים כדי לתמוך במערך המבצעי של צהל ולנצח את המלחמה הבאה "/>
      </GridList>
      </div>
    );
}

export default JobsList;