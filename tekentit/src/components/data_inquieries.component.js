import React, { Component, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { Typography } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';


const Data = props => {
    const classes = useStyles();

    return(
        <div>
        <Card>
            <CardActionArea>
                <CardContent>
                    <Typography edge="end" variant="h5" component="h2">
                        ישראל ישראלי 
                    </Typography>
                    <Typography>
                        <List >
                            <ListItem>
                                <ListItemText primary = "8444645" className={classes.txt}/>
                            </ListItem>
                            <Divider/>
                            <ListItem>
                                <ListItemText primary = "סגן" className={classes.txt}/>
                            </ListItem>
                            <Divider/>
                            <ListItem>
                                <ListItemText primary = "ראש צוות פיתוח" className={classes.txt}/>
                            </ListItem>
                            <Divider/>
                            <ListItem>
                                <ListItemText primary = "0552255419" className={classes.txt}/>
                            </ListItem>
                        </List>
                    </Typography>
                </CardContent>
            </CardActionArea>
            <CardActions>
                <Button className={classes.btn1} variant="contained"> מחק </Button>
                <Button className={classes.btn2} variant="contained"> פרטים נוספים </Button>
            </CardActions>
        </Card>
        </div>
    )
}

const useStyles = makeStyles((theme) => ({
    txt:{
        color:'grey',
        fontSize:33,
        direction:'rtl',
        display :'flex',
        alignItems:'center',
        justifyContent:'center',
        flex: 1

    },
    buttons:{
        justifyContent:'space-between',
        alignItems:'center',
        display:'flex',
        flexDirection:'row',
    },
    btn1:{
        color:'white',
        fontSize:10,
        alignItems:'center',
        justifyContent:'center',
        border: '1px solid',
        marginLeft:10,
        backgroundColor: '#bdbdbd',
    },
    btn2:{
        color:'white',
        fontSize:10,
        alignItems:'center',
        justifyContent:'center',
        border: '1px solid',
        backgroundColor:'black',
        marginRight:10,
    }
}));
export default Data