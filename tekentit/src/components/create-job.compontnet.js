import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AdminJob from "../components/admin-job.component";
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
      

    },
    job: {
        padingRight: theme.spacing(1),
        marginBottom: theme.spacing(1)
    },
    btn: {
        marginRight: 125,
        marginBottom: 15,
        backgroundColor:'#db8266'
    }
  }));

const CreateJob = () => {

    const classes = useStyles();

    return (
        <div>
            <Link to={"/new/"}>
<Button className={classes.btn} variant="outlined">הוסף תקן</Button>
</Link>

<GridList  className={classes.root} cols={3} >

<AdminJob title="ראש צוות פיתוח" content="עיסוק הצוות העיקרי מיפוי ולוגיקות על מפות, בנוסף לכך פיתוח ויב בטכנולגיות חדשניות"/>
                 <AdminJob title="קצין פרויקט" content="קצין פרויקט הכי חשוב בצבא שמטרתו תכלס כלום, אני ממשיך לכתוב כי אני צריך מלל ואני עדיין כותה תודה"/> 
                  <AdminJob title="מנהל מוצר" content="המוצר הכי שווה מבין כל המוצרים האחרים שווה לך לבוא אלינו ולהתרשם מהשירות האיכות והמענה המהיר תודה"/>
                 <AdminJob title="ראש צוות דאטה" content="כמה דאטה מלא דברים מגניבים בעיקר דאטה ועוד דאטה ועוד דאטה ועוד דאטה זהו יש מספיק מלל"/>
                 <AdminJob title="ראש שצוות Data Analyst" content="תפקיד בתחום הדאטה. המטרה לדעת את כל המידה שצריך"/>
                

          
      </GridList>
    
        </div>
    );
}

export default CreateJob;