import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import { Toolbar } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { Link } from 'react-router-dom';
import HomeIcon from '@material-ui/icons/Home';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginBottom: 25
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        
    },
    appBar: {
        background: '#878787'
    },
    button: {
        margin: theme.spacing(1),
      },
      accountBtn:{
        marginLeft: theme.spacing(140)
      }
}));

const Navbar = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}  dir="ltr">   
            <AppBar position="static" className={classes.appBar}>
                <Toolbar>

                <Link to={"/add/"}>
                <Button
                        variant="contained"
                        
                        className={classes.button}
                        endIcon={<AddIcon/>}
                        edge="start"
                    >
                        תקנים שפתחתי
                    </Button>

                    
                </Link>

                     
                <Link to={"/profile/"}>
            <IconButton className={classes.accountBtn} color="#6bd4db" edge="end">
                
          <AccountCircleIcon />
        </IconButton>
        </Link>

        
        </Toolbar>
            </AppBar>
        </div>    
    );
}

export default Navbar;