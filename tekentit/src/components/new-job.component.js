import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import NotesIcon from '@material-ui/icons/Notes';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(0.1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),

  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(2),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  btn: {
    margin : 5
  }
}));

export default function NewJob() {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs" >
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <NotesIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          תקן חדש
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} >
              <TextField
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="שם התקן"
                autoFocus
              />
            </Grid>
            <Grid item xs={12} >
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="תיאור התקן"
                name="lastName"
                autoComplete="lname"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="דרגה דרושה"
              />
            </Grid>
            <Grid item xs={12}>
            <Select
          labelId="demo-simple-select-outlined-label"
          id="demo-simple-select-outlined"
          label="מיקום גיגוארפי"
          fullWidth
        >
          <MenuItem value="">
          <em>מיקום גיגוארפי</em>
          </MenuItem>
          <MenuItem value={10}>חיפה</MenuItem>
          <MenuItem value={20}>תל אביב</MenuItem>
          <MenuItem value={30}>מצפה רמון</MenuItem>
        </Select>
            </Grid>
            <Grid item xs={12}>
            <TextField
    id="date"
    label="תאריך תחילת תפקיד"
    type="date"
    fullWidth
    className={classes.textField}
    InputLabelProps={{
      shrink: true,
    }}
  />
            </Grid>
            <Grid item xs={12}>
            <TextField
    id="date"
    label="תאריך סיום תפקיד"
    type="date"
    fullWidth
    className={classes.textField}
    InputLabelProps={{
      shrink: true,
    }}
  />
            </Grid>
          </Grid >
          
          <Grid item xs={12} sm={6}>
          <Link to={"/add/"}>
          <Button
            fullWidth
            variant="outlined"
            color="#46a9cb"
            className={classes.btn}
          >
            פרסם
          </Button>
          </Link>
          </Grid>
         
          <Grid item xs={12} sm={6}>
          <Link to={"/add/"}>
          <Button
            fullWidth
            variant="outlined"
            color="#46a9cb"
            className={classes.btn}
          >
            ביטול
          </Button>
          </Link>
          </Grid>
          
          
        </form>
      </div>

    </Container>
  );
}