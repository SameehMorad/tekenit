import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import CardMedia from '@material-ui/core/CardMedia';
import Divider from '@material-ui/core/Divider';
import GridList from '@material-ui/core/GridList';
import Data from './data_inquieries.component.js';


const Inquieries = () => {
    const classes = useStyles();
    //const [jobs, setJobs] = useState([]);
    return (
        <div className={classes.root}>
            <div className={classes.top}>
                <div className={classes.title}>
                    <Avatar src="../pictures/telechargement.jpg" className={classes.picture} />
                    <h className={classes.name}>
                        שלום יוסי!
                        </h>
                </div>
                <h className={classes.results}>
                    תוצאות : 1
                </h>
                <div className={classes.title2}>
                   
                    <h1 className={classes.role}>
                       תקן: ראש צוות פיתוח
                    </h1>
                </div>
            </div>
            <Divider />
            <div className={classes.grid}>
                <GridList className={classes.gridL} >
                    <Data />
                </GridList>
            </div>

        </div>
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        direction: "rtl",
        marginTop: 25,
        marginRight: 15,
        marginBottom: 25
        //marginLeft: theme.spacing(120),
    },
    results: {
        color: 'black',
        fontSize: 20,
        fontWeight: 500,
        marginTop:170,
        marginRight:-190,
        marginBottom:25
    },
    top: {
        display: 'flex',
        flexDirection: 'row',
        //justifyContent:'space-between',
    },
    grid: {
        marginTop: 25,
        marginRight: 15,
    },
    gridL: {
        //marginTop: 50,
        display: 'flex',
        flexWrap: 'wrap',
        wight: 500,
        //justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    entrevue: {
        height: '130px',
        wight: '200px',
    },
    title: {
        display: 'flex',
        flexDirection: 'row',
        //justifyContent:'space-between'
    },
    role: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 30,
        //marginRight:100
    },
    name: {
        color: 'black',
        fontSize: 25,
        fontWeight: 500,
        alignSelf: 'center',
        marginRight: 25,
        alignItems: 'center',
        marginBottom: 90
    },
    picture: {
        height: '110px',
        width: '110px',
    },
    title2: {
        display: 'flex',
        flexDirection: 'column',
        //justifyContent:'center',
        alignItems: 'center',
        marginRight: 340
    },
}));

export default Inquieries;
