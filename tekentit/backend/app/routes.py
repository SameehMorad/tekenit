import json

from flask import jsonify, request

from app import app
from app.__init__ import db
from app.models import Assignment, Candidacy


@app.route('/assignments',  methods=('GET', 'POST'))
def assignments():
    list = Assignment.query.all()
    d = {}

    for assignment in list:
        print(assignment.json())
        response_read = str(assignment.json().data.decode('utf-8'))
        d[assignment.id] = json.loads(response_read.replace("\"", '"'))



    return jsonify(d)


@app.route('/my_candidacy', methods=('GET', 'POST'))
def my_candidacy():
    personal_number = request.get_json()
    list = Candidacy.query.all()
    d = {}

    for candidacy in list:
        if candidacy.personal_number == personal_number:
            d[candidacy.id] = str(candidacy.json().data)

    return jsonify(d)


@app.route('/index')
def index():
    return "Hello, World!"