from flask import jsonify
from sqlalchemy import PrimaryKeyConstraint

from app import db


class Rank(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64))


class User(db.Model):
    personal_number = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(64), index=True, unique=True)
    last_name = db.Column(db.String(64), index=True, unique=True)
    rank = db.Column(db.Integer, db.ForeignKey('rank.id'))
    current_assignment = db.Column(db.String(128), index=True)
    location = db.Column(db.String(64), index=True)
    contacts = db.Column(db.String(64), index=True)

    def json(self):
        return {
            {"personal_number", self.personal_number},
            {"first_name", self.first_name},
            {"last_name", self.last_name},
            {"rank", self.rank},
            {"current_assignment", self.current_assignment},
            {"location", self.location},
            {"contacts", self.contacts}
        }


class Assignment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), index=True, unique=True)
    advertiser = db.Column(db.Integer, db.ForeignKey('user.personal_number'))
    required_rank = db.Column(db.Integer, db.ForeignKey('rank.id'))
    begging_date = db.Column(db.Date)
    ending_date = db.Column(db.Date)

    def json(self):
        return jsonify(
            {"id": self.id},
            {"title": self.title},
            {"advertiser": self.advertiser},
            {"required_rank": self.required_rank},
            {"begging_date": self.begging_date},
            {"ending_date": self.ending_date},
        )


class Candidacy(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer, db.ForeignKey('user.personal_number'))
    assignment = db.Column(db.Integer, db.ForeignKey('assignment.id'))
    status = db.Column(db.Boolean, default=False)

    def json(self):
        return jsonify(
            {"id": self.id},
            {"user": self.user},
            {"assignment": self.assignment},
            {"status": self.status}
        )
